<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title;?></title>
    <!-- load bootstrap css file --> 
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4"><h1><?php echo $content;?></h1>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First</th>
                            <th scope="col">Last</th>
                            <th scope="col">Handle</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Naufal</td>
                            <td>Faishal</td>
                            <td>@stis</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Jasmine</td>
                            <td>Abqoriyyah</td>
                            <td>@stis</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Deva</td>
                            <td>Zalfa</td>
                            <td>@stis</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- load query js file -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</body>
</html>